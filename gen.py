import pandas as pd
import json
from random import randint, uniform

def generate(count,data,Range):
	store = []
	for index in range(count):
		moisture =data['moisture'] + randint(-Range['moisture'],Range['moisture'])
		ph =data['ph'] + uniform(-Range['ph'],Range['ph'])
		temperature =data['temperature'] + uniform(-Range['temperature'],Range['temperature'])
		luminosity =data['luminosity'] + randint(-Range['luminosity'],Range['luminosity'])
		time = str(pd.to_datetime(data['time']) + pd.Timedelta(minutes=randint(-Range['time'],Range['time'])))
		store.append({'moisture':moisture,'ph':ph,'temperature':temperature,'luminosity':luminosity,'time':time,'crop':data['crop']})
	return store
SUGAR = {'moisture':200,'ph':6.5,'temperature':38,'luminosity':100,'time':'2019-02-15T11:32:02Z','crop':'sugar'}
PADDY = {'moisture':220,'ph':6.0,'temperature':35,'luminosity':110,'time':'2019-02-17T11:32:02Z','crop':'paddy'}
BANANA = {'moisture':170,'ph':7.0,'temperature':30,'luminosity':80,'time':'2019-02-18T11:32:02Z','crop':'banana'}
COCONUT = {'moisture':180,'ph':6.5,'temperature':28,'luminosity':90,'time':'2019-02-20T11:32:02Z','crop':'coconut'}

RANGE={'moisture':5,'ph':0.3,'temperature':3,'luminosity':8,'time':300}

sugar_data = pd.DataFrame.from_dict(generate(131807,SUGAR,RANGE))
paddy_data = pd.DataFrame.from_dict(generate(112174,PADDY,RANGE))
banana_data = pd.DataFrame.from_dict(generate(141877,BANANA,RANGE))
coconut_data = pd.DataFrame.from_dict(generate(132089,COCONUT,RANGE))

all_data=pd.concat([sugar_data, paddy_data, banana_data, coconut_data], ignore_index=True)
train_data=all_data.sample(frac=1)
test_data=all_data.sample(frac=.2)

train_data.to_csv('train.csv', index=False)

test_data.to_csv('test.csv', index=False)
