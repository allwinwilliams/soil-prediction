import main
import numpy as np
logistic_classifier = main.train_logistic()
svm_classifier = main.train_svm()

print("Enter data....")
moisture = float(input("enter moisture value::"))
ph = float(input("enter ph value::"))
temperature = float(input("enter temperature value::"))
luminosity = float(input("enter luminosity value::"))

crop = logistic_classifier.predict([[moisture, ph, temperature, luminosity]])
print("\n\nPREDICTED CROP BY MULTIVARIATE LOGISTIC: ", crop)

crop = svm_classifier.predict([[moisture, ph, temperature, luminosity]])
print("\n\nPREDICTED CROP BY MULTIVARIATE SVM: ", crop)