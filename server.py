from flask import Flask, request
import main
import json

app = Flask(__name__)

logistic_classifier = main.train_logistic()
svm_classifier = main.train_svm()
nn_classifier = main.train_nn()

# capsules = main.train_capsule()

@app.route("/logistic")
def logistic():
    moisture = float(request.args.get('moisture'))
    ph = float(request.args.get('ph'))
    temperature = float(request.args.get('temperature'))
    luminosity = float(request.args.get('luminosity'))
    return str(logistic_classifier.predict([[moisture, ph, temperature, luminosity]]))

@app.route("/svm")
def svm():
    moisture = float(request.args.get('moisture'))
    ph = float(request.args.get('ph'))
    temperature = float(request.args.get('temperature'))
    luminosity = float(request.args.get('luminosity'))
    return str(svm_classifier.predict([[moisture, ph, temperature, luminosity]]))

@app.route("/nn")
def nn():
     moisture = float(request.args.get('moisture'))
     ph = float(request.args.get('ph'))
     temperature = float(request.args.get('temperature'))
     luminosity = float(request.args.get('luminosity'))
     return str(svm_classifier.predict([[moisture, ph, temperature, luminosity]]))


# @app.route("/capsule")
# def capsule():
#     moisture = float(request.args.get('moisture'))
#     ph = float(request.args.get('ph'))
#     temperature = float(request.args.get('temperature'))
#     luminosity = float(request.args.get('luminosity'))
#     return str(main.combine_clf(capsules, moisture, ph, temperature, luminosity))



app.run()
