from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier

from sklearn.metrics import confusion_matrix, accuracy_score

import numpy as np
import pandas as pd
import const

from collections import defaultdict

train_data = pd.read_csv('train.csv')
test_data = pd.read_csv('test.csv')

def normaliser(data):
	data = np.array(data)

	return ((data - np.mean(data,axis=0)) / np.amax(data,axis=0) - np.amin(data,axis=0))

def train_logistic():
	print('==========MULTIVARIATE LOGISTIC REGRESSION==============')

	logistic_clf = LogisticRegression(random_state=0, solver='lbfgs', max_iter=15000, multi_class='multinomial')


	logistic_clf.fit(normaliser(train_data.drop(columns=['crop', 'time'])), train_data['crop'])

	test_predictions = logistic_clf.predict(test_data.drop(columns=['crop', 'time']))
	accuracy = accuracy_score(test_data['crop'], test_predictions)

	print('SCORE::')
	print(accuracy)

	cm = confusion_matrix(test_data['crop'], test_predictions)
	print('CONFUSION MATRIX::')
	print(cm)

	precision = np.mean(np.diag(cm) / np.sum(cm, axis = 0))
	print('precision')
	print(precision)

	recall = np.mean(np.diag(cm) / np.sum(cm, axis = 1))
	print('recall')
	print(recall)

	f1 = 2 * (precision * recall / (precision + recall))
	print('f1 score::')
	print(f1)

	return logistic_clf

def train_svm():
	print('\n\n==========SUPPORT VECTOR MACHINE==============')

	svm_clf = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)
	svm_clf.fit(normaliser(train_data.drop(columns=['crop', 'time'])), train_data['crop'])

	test_predictions = svm_clf.predict(test_data.drop(columns=['crop', 'time']))
	accuracy = accuracy_score(test_data['crop'], test_predictions)

	print('SCORE::')
	print(accuracy)

	cm = confusion_matrix(test_data['crop'], test_predictions)
	print('CONFUSION MATRIX::')
	print(cm)

	precision = np.mean(np.diag(cm) / np.sum(cm, axis = 0))
	print('precision')
	print(precision)

	recall = np.mean(np.diag(cm) / np.sum(cm, axis = 1))
	print('recall')
	print(recall)

	f1 = 2 * (precision * recall / (precision + recall))
	print('f1 score::')
	print(f1)

	return svm_clf

def train_nn():
	print('\n\n==========NUERAL NETWORK==============')

	nn_clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1, max_iter=15000)
	nn_clf.fit(normaliser(train_data.drop(columns=['crop', 'time'])), train_data['crop'])

	test_predictions = nn_clf.predict(test_data.drop(columns=['crop', 'time']))
	accuracy = accuracy_score(test_data['crop'], test_predictions)

	print('SCORE::')
	print(accuracy)

	cm = confusion_matrix(test_data['crop'], test_predictions)
	print('CONFUSION MATRIX::')
	print(cm)

	precision = np.mean(np.diag(cm) / np.sum(cm, axis = 0))
	print('precision')
	print(precision)

	recall = np.mean(np.diag(cm) / np.sum(cm, axis = 1))
	print('recall')
	print(recall)

	f1 = 2 * (precision * recall / (precision + recall))
	print('f1 score::')
	print(f1)

	return nn_clf


def train_capsule():
	print('\n\n==========CAPSULATED SUPPORT VECTOR MACHINE==============')

	svm_clf1 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf1.fit(normaliser(train_data.drop(columns=['crop', 'time'])), train_data['crop'])

	svm_clf2 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf2.fit(normaliser(train_data.drop(columns=['ph','crop', 'time'])), train_data['crop'])

	svm_clf3 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf3.fit(normaliser(train_data.drop(columns=['temperature','crop', 'time'])), train_data['crop'])

	svm_clf4 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf4.fit(normaliser(train_data.drop(columns=['luminosity','crop', 'time'])), train_data['crop'])

	svm_clf5 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf5.fit(normaliser(train_data.drop(columns=['moisture','crop', 'time'])), train_data['crop'])

	svm_clf6 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf6.fit(normaliser(train_data.drop(columns=['temperature','luminosity','moisture','crop', 'time'])), train_data['crop'])

	svm_clf7 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf7.fit(normaliser(train_data.drop(columns=['temperature','luminosity','ph','crop', 'time'])), train_data['crop'])

	svm_clf8 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf8.fit(normaliser(train_data.drop(columns=['temperature','moisture','ph','crop', 'time'])), train_data['crop'])

	svm_clf9 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf9.fit(normaliser(train_data.drop(columns=['luminosity','moisture','ph','crop', 'time'])), train_data['crop'])

	svm_clf10 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf10.fit(normaliser(train_data.drop(columns=['temperature','luminosity','crop', 'time'])), train_data['crop'])

	svm_clf11 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf11.fit(normaliser(train_data.drop(columns=['temperature','ph','crop', 'time'])), train_data['crop'])

	svm_clf12 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf12.fit(normaliser(train_data.drop(columns=['moisture','ph','crop', 'time'])), train_data['crop'])

	svm_clf13 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf13.fit(normaliser(train_data.drop(columns=['temperature','moisture','crop', 'time'])), train_data['crop'])

	svm_clf14 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf14.fit(normaliser(train_data.drop(columns=['luminosity','moisture','crop', 'time'])), train_data['crop'])

	svm_clf15 = LinearSVC(random_state=0, tol=1e-5, max_iter=15000)

	svm_clf15.fit(normaliser(train_data.drop(columns=['ph','luminosity','crop', 'time'])), train_data['crop'])

	svm_cmb_clfs=[svm_clf1,svm_clf2,svm_clf3,svm_clf4,svm_clf5,svm_clf6,svm_clf7,svm_clf8,svm_clf9,svm_clf10,svm_clf11,svm_clf12,svm_clf13,svm_clf14,svm_clf15]

	capsule_accuracy(svm_cmb_clfs)

	return svm_cmb_clfs


def combine_clf(clfs, moisture, ph, temperature, luminosity):
	# results = []
	#
	# results.append(clfs[0].predict([[moisture, ph, temperature, luminosity]]))
	# results.append(clfs[1].predict([[moisture, temperature, luminosity]]))
	# results.append(clfs[2].predict([[moisture, ph, luminosity]]))
	# results.append(clfs[3].predict([[moisture, ph, temperature]]))
	# results.append(clfs[4].predict([[ph, temperature, luminosity]]))
	# results.append(clfs[5].predict([[ph]]))
	# results.append(clfs[6].predict([[moisture]]))
	# results.append(clfs[7].predict([[luminosity]]))
	# results.append(clfs[8].predict([[temperature]]))
	# results.append(clfs[9].predict([[moisture, ph]]))
	# results.append(clfs[10].predict([[moisture, luminosity]]))
	# results.append(clfs[11].predict([[temperature, luminosity]]))
	# results.append(clfs[12].predict([[ph, luminosity]]))
	# results.append(clfs[13].predict([[ph, temperature]]))
	# results.append(clfs[14].predict([[moisture, temperature]]))


	# combine = defaultdict(int)
	#
	# for i in results:
	# 	if i[0] not in combine:
	# 		combine[i[0]] = 0
	#
	# 	combine[i[0]] += 1
	#
	#
	# max_key = results[0][0]
	# max_value = 0
	# for key in combine:
	# 	if combine[key] > max_value:
	# 		max_key = key
	# 		max_value = combine[key]

	# return np.array([max_key])

	combine = defaultdict(int)

	p = clfs[5].predict([[ph]])[0]
	m = clfs[6].predict([[moisture]])[0]
	l = clfs[7].predict([[luminosity]])[0]
	t = clfs[8].predict([[temperature]])[0]
	combine[p]+=0.25
	combine[m]+=0.25
	combine[l]+=0.25
	combine[t]+=0.25

	pm=clfs[9].predict([[moisture, ph]])[0]
	combine[pm]+=0.75
	if pm==p:
		combine[p]+=.5
	else:
		combine[p]-=.25

	if pm==m:
		combine[m]+=.5
	else:
		combine[m]-=.25

	pl=clfs[12].predict([[ph, luminosity]])[0]
	combine[pl]+=0.75
	if pl==p:
		combine[p]+=.5
	else:
		combine[p]-=.25

	if pl==l:
		combine[l]+=.5
	else:
		combine[l]-=.25

	pt=clfs[13].predict([[ph, temperature]])[0]
	combine[pt]+=0.75
	if pt==p:
		combine[p]+=.5
	else:
		combine[p]-=.25

	if pt==t:
		combine[t]+=.5
	else:
		combine[t]-=.25

	ml=clfs[10].predict([[moisture, luminosity]])[0]
	combine[pm]+=0.75
	if ml==m:
		combine[m]+=.5
	else:
		combine[m]-=.25

	if ml==l:
		combine[l]+=.5
	else:
		combine[l]-=.25

	mt=clfs[14].predict([[moisture, temperature]])[0]
	combine[mt]+=0.75
	if mt==m:
		combine[m]+=.5
	else:
		combine[m]-=.25

	if mt==t:
		combine[t]+=.5
	else:
		combine[t]-=.25

	lt=clfs[11].predict([[temperature, luminosity]])[0]
	combine[lt]+=0.75
	if lt==l:
		combine[l]+=.5
	else:
		combine[l]-=.25

	if lt==t:
		combine[t]+=.5
	else:
		combine[t]-=.25

	pmt=clfs[3].predict([[moisture, ph, temperature]])[0]
	combine[pmt]+=0.75*1.5
	if pmt==pm:
		combine[pm]+=0.5*1.5
	else:
		combine[pm]-=0.25*1.5

	if pmt==pt:
		combine[pt]+=0.5*1.5
	else:
		combine[pt]-=0.25*1.5

	if pmt==mt:
		combine[mt]+=0.5*1.5
	else:
		combine[mt]-=0.25*1.5

	pml=clfs[2].predict([[moisture, ph, luminosity]])[0]
	combine[pml]+=0.75*1.5
	if pml==pm:
		combine[pm]+=0.5*1.5
	else:
		combine[pm]-=0.25*1.5

	if pml==pl:
		combine[pl]+=0.5*1.5
	else:
		combine[pl]-=0.25*1.5

	if pml==ml:
		combine[ml]+=0.5*1.5
	else:
		combine[ml]-=0.25*1.5

	plt=clfs[4].predict([[ph, temperature, luminosity]])[0]
	combine[plt]+=0.75*1.5
	if plt==pl:
		combine[pl]+=0.5*1.5
	else:
		combine[pl]-=0.25*1.5

	if plt==pt:
		combine[pt]+=0.5*1.5
	else:
		combine[pt]-=0.25*1.5

	if plt==lt:
		combine[lt]+=0.5*1.5
	else:
		combine[lt]-=0.25*1.5

	mlt=clfs[1].predict([[moisture, temperature, luminosity]])[0]
	combine[mlt]+=0.75*1.5
	if mlt==ml:
		combine[ml]+=0.5*1.5
	else:
		combine[ml]-=0.25*1.5

	if mlt==lt:
		combine[lt]+=0.5*1.5
	else:
		combine[lt]-=0.25*1.5

	if mlt==mt:
		combine[mt]+=0.5*1.5
	else:
		combine[mt]-=0.25*1.5


	pmlt=clfs[0].predict([[moisture, ph, temperature, luminosity]])[0]
	combine[pmlt]+=0.75*1.5*1.5
	if pmlt==pml:
		combine[pml]+=.5*1.5*1.5
	else:
		combine[pml]-=.5*1.5*1.5

	if pmlt==plt:
		combine[plt]+=.5*1.5*1.5
	else:
		combine[plt]-=.5*1.5*1.5

	if pmlt==pmt:
		combine[pmt]+=.5*1.5*1.5
	else:
		combine[pmt]-=.5*1.5*1.5

	if pmlt==mlt:
		combine[mlt]+=.5*1.5*1.5
	else:
		combine[mlt]-=.5*1.5*1.5

	max_value = 0

	for key in combine:
		if combine[key] > max_value:
			max_key = key
			max_value = combine[key]

	return np.array([max_key])


















def capsule_accuracy(clfs):
	tp = 0
	total = 0
	for i in test_data.index:
		total += 1
		if combine_clf(clfs, test_data.ix[i]['moisture'], test_data.ix[i]['ph'], test_data.ix[i]['temperature'], test_data.ix[i]['luminosity'])[0] == test_data.ix[i]['crop']:
			tp += 1

	accuracy = tp / total

	print('accuracy')
	print(accuracy)
